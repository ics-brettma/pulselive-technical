package com.pulselive.technicaltest.model;

import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * Immutable value object describing the result of a game of football between
 * two teams.
 * 
 * @author Mat Brett
 * @since 0.0.1
 */
public class Match {

	final private String homeTeam;
	final private String awayTeam;
	final private int homeScore;
	final private int awayScore;

	public Match(final String homeTeam, final String awayTeam,
			final int homeScore, final int awayScore) {

		this.homeTeam = homeTeam;
		this.awayTeam = awayTeam;
		this.homeScore = homeScore;
		this.awayScore = awayScore;
	}

	public String getHomeTeam() {
		return homeTeam;
	}

	public String getAwayTeam() {
		return awayTeam;
	}

	public int getHomeScore() {
		return homeScore;
	}

	public int getAwayScore() {
		return awayScore;
	}

	@Override
	/**
	 * Custom hashing algorithm to complement the overriden equals criteria.
	 */
	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(homeTeam).append(awayTeam)
				.toHashCode();
	}

	/**
	 * Matches are equal if they are both {@code Match} instances and have the
	 * same home and away teams.
	 * 
	 * @param obj
	 *            Object to compare to this instance of {@code Match}
	 */
	@Override
	public boolean equals(Object obj) {
		return (null != obj
				&& obj instanceof Match
				&& (null != this.getHomeTeam() && this.getHomeTeam()
						.equalsIgnoreCase(((Match) obj).getHomeTeam())) && (null != this
				.getAwayTeam() && this.getAwayTeam().equalsIgnoreCase(
				((Match) obj).getAwayTeam())));
	}

}
