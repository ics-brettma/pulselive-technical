package com.pulselive.technicaltest.model;

/**
 * Immutable value object representing a football team's row in a League table.
 * 
 * @author Mat Brett
 * @since 0.0.1
 *
 */
public class LeagueTableEntry {

	final private String teamName;
	final private int played;
	final private int won;
	final private int drawn;
	final private int lost;
	final private int goalsFor;
	final private int goalsAgainst;
	final private int goalDifference;
	final private int points;

	public LeagueTableEntry(String teamName, int played, int won, int drawn,
			int lost, int goalsFor, int goalsAgainst, int goalDifference,
			int points) {

		this.teamName = teamName;
		this.played = played;
		this.won = won;
		this.drawn = drawn;
		this.lost = lost;
		this.goalsFor = goalsFor;
		this.goalsAgainst = goalsAgainst;
		this.goalDifference = goalDifference;
		this.points = points;
	}

	public String getTeamName() {
		return teamName;
	}

	public int getPlayed() {
		return played;
	}

	public int getWon() {
		return won;
	}

	public int getDrawn() {
		return drawn;
	}

	public int getLost() {
		return lost;
	}

	public int getGoalsFor() {
		return goalsFor;
	}

	public int getGoalsAgainst() {
		return goalsAgainst;
	}

	public int getGoalDifference() {
		return goalDifference;
	}

	public int getPoints() {
		return points;
	}
}
