package com.pulselive.technicaltest.services;

import java.util.Comparator;

import org.springframework.stereotype.Service;

import com.pulselive.technicaltest.model.LeagueTableEntry;

/**
 * Class which ensures that sorts two {@code LeagueTableEntry} instances by
 * points, goal difference, goals for, and then team names.
 * 
 * @author Mat Brett
 * @since 0.0.1
 *
 */
@Service
public class LeagueTableSorter implements Comparator<LeagueTableEntry> {

	@Override
	public int compare(LeagueTableEntry teamA, LeagueTableEntry teamB) {

		if (null == teamA || null == teamB) {
			throw new NullPointerException();
		}

		int comparator = sortBy(teamA.getPoints(), teamB.getPoints());

		if (comparator == 0) {
			comparator = sortBy(teamA.getGoalDifference(),
					teamB.getGoalDifference());
		}

		if (comparator == 0) {
			comparator = sortBy(teamA.getGoalsFor(), teamB.getGoalsFor());
		}

		if (comparator == 0) {
			comparator = teamA.getTeamName().compareTo(teamB.getTeamName());
			// contract on string comparator is backwards
		}

		return comparator;

	}

	/**
	 * Re-usable integer comparison function.
	 * 
	 * @param teamAValue
	 *            Left operand
	 * @param teamBValue
	 *            Right operand
	 * @return 1 if A is greater than B, -1 if B is greater than A, 0 if A==B
	 */
	private int sortBy(int teamAValue, int teamBValue) {

		int comparator = 0;
		// if team A has more points than team B then rank team A above team B
		if (teamAValue < teamBValue) {
			comparator = 1;
		}

		// if team A has less points than team B then rank team B above team A
		if (teamAValue > teamBValue) {
			comparator = -1;
		}
		return comparator;

	}
}
