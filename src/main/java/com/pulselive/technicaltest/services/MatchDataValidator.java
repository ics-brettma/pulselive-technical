package com.pulselive.technicaltest.services;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.pulselive.technicaltest.exception.IllegalMatchDataException;
import com.pulselive.technicaltest.model.Match;

/**
 * Class which ensures that the collection of {@code Match} objects which are
 * used by the {@code LeagueTableCreator} as source data to create a league
 * table from is sane.
 * 
 * @author Mat Brett
 * @since 0.0.1
 *
 */
@Service
public class MatchDataValidator {

	/**
	 * Throws an {@code IllegalMatchDataException} if the collection of match
	 * data provided is null or empty, if a fixture is malformed (contains null
	 * or empty team names), if a team is trying to play themselves in a
	 * fixture, or if a fixture appears twice in the league (irrespective of
	 * score)
	 * 
	 * @param matches
	 *            Collection of match data to be validated
	 */
	public final List<Match> validate(final List<Match> matches) {
		// ensure fixture collection has at least one fixture in it
		if (null == matches || matches.isEmpty()) {
			throw new IllegalMatchDataException(
					"Could not produce a league table - no matches were provided");
		}
		// ensure fixture collection does not contain any duplicates
		Set<Match> uniqueMatches = new HashSet<Match>(matches);
		if (uniqueMatches.size() != matches.size()) {
			throw new IllegalMatchDataException(
					"Could not produce a league table - the match data contains the same fixture more than once");
		}

		for (Match m : matches) {
			// ensure home and away team information is not null or empty
			if (null == m.getHomeTeam() || m.getHomeTeam().isEmpty()
					|| null == m.getAwayTeam() || m.getAwayTeam().isEmpty()) {
				throw new IllegalMatchDataException(
						"Could not produce a league table - the match data contains a fixture with a blank team name");

			}
			// ensure fixture collection does not contain any fixtures where
			// teams
			// are trying to play themselves
			if (m.getAwayTeam().equalsIgnoreCase(m.getHomeTeam())) {
				throw new IllegalMatchDataException(
						"Could not produce a league table - the match data contains a fixture where a team is playing itself");
			}
		}
		return matches;
	}
}
