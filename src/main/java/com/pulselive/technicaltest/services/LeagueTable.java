package com.pulselive.technicaltest.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.pulselive.technicaltest.model.LeagueTableEntry;
import com.pulselive.technicaltest.model.Match;

/**
 * Legacy facade to a {@code LeagueTableGenerator} which returns a sorted league
 * table.
 * 
 * @author Mat Brett
 * @since 0.0.1
 *
 */
public class LeagueTable {

	private LeagueTableCreator leagueTableCreator;
	private final List<Match> matches;

	/**
	 * 
	 * Create a league table from the supplied list of match results
	 * 
	 * @param matches
	 *            Source football fixtures data
	 */

	@Autowired
	public LeagueTable(final List<Match> matches) {
		this.matches = matches;
	}

	/**
	 * Get the ordered list of league table entries for this league table.
	 * 
	 * @return List<LeagueTableEntry> a sorted league table
	 */

	public List<LeagueTableEntry> getTableEntries() {
		return leagueTableCreator.createLeagueTableFromMatchData(matches);
	}

	public LeagueTableCreator getLeagueTableCreator() {
		return leagueTableCreator;
	}

	@Autowired
	public void setLeagueTableCreator(LeagueTableCreator leagueTableCreator) {
		this.leagueTableCreator = leagueTableCreator;
	}
}
