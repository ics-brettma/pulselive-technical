package com.pulselive.technicaltest.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pulselive.technicaltest.model.LeagueTableEntry;
import com.pulselive.technicaltest.model.Match;

/**
 * Class which parses over a collection of {@code Match} objects to create a
 * League Table.
 * 
 * This class depends upon an instance of {@code MatchDataValidator} to validate
 * the integrity of the Match instances before a League Table is attempted.
 * 
 * This class depends upon an instance of {@code MatchDataSorter} to sort the
 * League Table standings by points, goal difference, goals for, and then team
 * names.
 * 
 * @author Mat Brett
 * @since 0.0.1
 *
 */
@Service
public class LeagueTableCreator {

	private MatchDataValidator matchDataValidator;
	private LeagueTableSorter leagueTableSorter;

	public List<LeagueTableEntry> createLeagueTableFromMatchData(
			List<Match> matchData) {
		matchData = matchDataValidator.validate(matchData);

		List<LeagueTableEntry> leagueTable = new ArrayList<LeagueTableEntry>();

		// derive a set of teams to iterate over
		Set<String> teams = new HashSet<String>();
		for (Match m : matchData) {
			if (!teams.contains(m.getHomeTeam())) {
				teams.add(m.getHomeTeam());
			}
			if (!teams.contains(m.getAwayTeam())) {
				teams.add(m.getAwayTeam());
			}
		}

		// work over the match data to create a
		// LeagueTableEntry for each team and add it to the league table
		for (String team : teams) {
			int played = 0;
			int won = 0;
			int drawn = 0;
			int lost = 0;
			int goalsFor = 0;
			int goalsAgainst = 0;

			int points;
			int goalDifference;

			for (Match m : matchData) {

				if (m.getHomeTeam().equals(team)) {
					// add to played tally
					played++;

					// add to win/drawn/lost tally
					if (m.getHomeScore() > m.getAwayScore()) {
						won++;
					}
					if (m.getHomeScore() == m.getAwayScore()) {
						drawn++;
					}
					if (m.getHomeScore() < m.getAwayScore()) {
						lost++;
					}

					goalsFor = goalsFor + m.getHomeScore();
					goalsAgainst = goalsAgainst + m.getAwayScore();
				}

				if (m.getAwayTeam().equals(team)) {
					// add to played tally
					played++;

					// add to win/drawn/lost tally
					if (m.getAwayScore() > m.getHomeScore()) {
						won++;
					}
					if (m.getAwayScore() == m.getHomeScore()) {
						drawn++;
					}
					if (m.getAwayScore() < m.getHomeScore()) {
						lost++;
					}

					goalsFor = goalsFor + m.getAwayScore();
					goalsAgainst = goalsAgainst + m.getHomeScore();
				}
			}

			points = (won * 3 + drawn);
			goalDifference = (goalsFor - goalsAgainst);

			leagueTable.add(new LeagueTableEntry(team, played, won, drawn,
					lost, goalsFor, goalsAgainst, goalDifference, points));
		}

		Collections.sort(leagueTable, leagueTableSorter);
		return leagueTable;
	}

	public MatchDataValidator getMatchDataValidator() {
		return matchDataValidator;
	}

	public LeagueTableSorter getLeagueTableSorter() {
		return leagueTableSorter;
	}

	@Autowired
	public void setMatchDataValidator(MatchDataValidator matchDataValidator) {
		this.matchDataValidator = matchDataValidator;
	}

	@Autowired
	public void setLeagueTableSorter(LeagueTableSorter leagueTableSorter) {
		this.leagueTableSorter = leagueTableSorter;
	}
}
