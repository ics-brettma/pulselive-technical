package com.pulselive.technicaltest.exception;

import com.pulselive.technicaltest.services.LeagueTable;

/**
 * This class extends {@code IllegalArgumentException} to provide a more
 * descriptive runtime exception for cases where {@code Match} instances
 * provided to the {@code LeagueTable} service are invalid, for example when an
 * empty set of matches are provided, if duplicate fixtures are provided in the
 * collection, and if a fixture is provided where it appears that a team is
 * playing itself.
 * 
 * @author Mat Brett
 * @since 0.0.1
 * @see IllegalArgumentException
 * @see LeagueTable
 * @see Match
 *
 */
@SuppressWarnings("serial")
public class IllegalMatchDataException extends IllegalArgumentException {

	/**
	 * Require a helpful error message and pass to super.
	 * 
	 * @param matchDataExceptionMessage
	 *            A helpful error message to describe a problem with one of more
	 *            instances of {@code Match}
	 */
	public IllegalMatchDataException(String matchDataExceptionMessage) {
		super(matchDataExceptionMessage);
	}
}
