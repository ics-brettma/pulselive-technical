package com.pulselive.technicaltest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Spring boot driver class for Mat's solution to the Pulse Live Java Developer
 * programming task.
 * 
 * @author Mat Brett
 * @since 0.0.1
 *
 */
@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
