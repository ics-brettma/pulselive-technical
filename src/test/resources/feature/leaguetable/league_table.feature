Feature: Sorted League Table
	As a football fan
	I want a fully sorted league table
	So that I can see how each football team is performing in the league

Scenario: Where legal match data is provided, return a league table which is sorted by points, goal difference, goals for and then team names. The normal rules for scoring points apply.
	Given a collection of match data from an imaginary league
	When two teams are equal on each of points, goal difference and goals for
	And two teams are equal on both points and goal difference, only
	And two teams are equal on points only
	When a league table is requested from the League Table service, a league table is returned
	And that league table is sorted by points in descending order
	And where two teams in the league have equal points, the teams are sorted instead by goal difference in descending order
	And where two teams in the league have equal points and equal goal difference, the teams are sorted by goals for, in descending order
	And where two teams in the league have equal points and equal goal difference and equal goals for, the teams are sorted by alphabetically, in ascending order

Scenario Outline: Where illegal match data is provided (no match data is provided), return an error message
	Given a collection of match data from an imaginary league
	And the match data provided is <empty>
	When a league table is requested of the League Table service, the message "Could not produce a league table - no matches were provided" is returned
	
	Examples:
		| empty    | 
		| null     |
		| empty    |                        

Scenario: Where illegal match data is provided (a fixture is included where a team plays itself), return an error message
	Given a collection of match data from an imaginary league
	And the match data provided contains an illegal match where a team is playing itself
    When a league table is requested of the League Table service, the message "Could not produce a league table - the match data contains a fixture where a team is playing itself" is returned

Scenario Outline: Where illegal match data is provided (a fixture is included which is malformed i.e. it has a null/empty home or away team name), throw an IllegalMatchDataException
	Given a collection of match data from an imaginary league
	And the match data provided contains an illegal match where the <home or away> team's name <has not been given>
    When a league table is requested of the League Table service, the message "Could not produce a league table - the match data contains a fixture with a blank team name" is returned
	
	Examples:
		| has not been given    | home or away |
		| is null           | home |
		| is empty          | home |
		| is null           | away |
		| is empty          | away |
		
#Note, I've made an assumption here that this is an English style league where teams don't play each other at home more than once per season!
Scenario: Where illegal match data is provided (a fixture is provided in a season twice), return an error message
	Given a collection of match data from an imaginary league
	And the match data provided contains a duplicate fixture
    When a league table is requested of the League Table service, the message "Could not produce a league table - the match data contains the same fixture more than once" is returned


