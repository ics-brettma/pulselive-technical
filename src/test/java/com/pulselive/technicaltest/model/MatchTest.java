package com.pulselive.technicaltest.model;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import org.junit.Test;

import com.pulselive.technicaltest.feature.leaguetable.Team;

/**
 * Unit tests for {code Match}
 * 
 * @author Mat Brett
 * @since 0.0.1
 *
 */
public class MatchTest {

	@Test
	public void testThatTwoMatchesWithTheSameHomeAndAwayTeamsAreEqualIrrespectiveOfScore() {
		Match basingstokeVsAldershot = new Match(Team.BASINGSTOKE.toString(),
				Team.ALDERSHOT.toString(), 4, 1);
		Match basingstokeVsAldershotAgain = new Match(
				Team.BASINGSTOKE.toString(), Team.ALDERSHOT.toString(), 3, 2);
		assertThat(basingstokeVsAldershot,
				is(equalTo(basingstokeVsAldershotAgain)));
	}
}
