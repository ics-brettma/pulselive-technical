package com.pulselive.technicaltest;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

/**
 * BDD execution context for all features of this application. The annotations
 * in this class are used to resolve the execution context's dependencies. The
 * context is invoked during the test phase of the maven build lifecycle.
 * 
 * @author Mat Brett
 * @since 0.0.1
 */
@RunWith(Cucumber.class)
@CucumberOptions(features = { "classpath:feature/" }, glue = { "com.pulselive.technicaltest.feature" })
public class ApplicationTest {

}
