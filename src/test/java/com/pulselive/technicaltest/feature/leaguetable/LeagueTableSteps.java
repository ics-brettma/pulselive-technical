package com.pulselive.technicaltest.feature.leaguetable;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalToIgnoringWhiteSpace;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

import java.util.ArrayList;
import java.util.List;

import com.pulselive.technicaltest.exception.IllegalMatchDataException;
import com.pulselive.technicaltest.model.LeagueTableEntry;
import com.pulselive.technicaltest.model.Match;
import com.pulselive.technicaltest.services.LeagueTable;
import com.pulselive.technicaltest.services.LeagueTableCreator;
import com.pulselive.technicaltest.services.LeagueTableSorter;
import com.pulselive.technicaltest.services.MatchDataValidator;

import cucumber.api.java.After;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

/**
 * This class contains the cucumber step definitions which implement - line by
 * line - the automated acceptance test scenarios described for the sorted
 * League table feature in league_table.feature. This class and its
 * corresponding feature file are dependencies of (see {@code ApplicationTest})
 * 
 * @author Mat Brett
 * @since 0.0.1
 */
public class LeagueTableSteps {

	/**
	 * After every scenario, reset the class state
	 */
	@After
	public void reset() {
		collectionOfMatches = null;
		leagueTableService = null;
		sortedLeagueTable = null;
	}

	/* shared state between steps within a single scenario */
	private List<Match> collectionOfMatches = null;
	private LeagueTable leagueTableService;
	private List<LeagueTableEntry> sortedLeagueTable = null;

	@Given("^a collection of match data from an imaginary league$")
	public void a_collection_of_match_data_from_an_imaginary_league()
			throws Throwable {
		collectionOfMatches = null;
	}

	@And("^the match data provided is (null|empty)$")
	public void the_match_data_provided_is_null_or_empty(String option)
			throws Throwable {

		if (option.equals("null")) {
			collectionOfMatches = null;
			assertThat(collectionOfMatches, is(nullValue()));
		} else if (option.equals("empty")) {
			collectionOfMatches = new ArrayList<Match>();
			assertThat(collectionOfMatches, is(empty()));
		}
	}

	@And("^the match data provided contains an illegal match where a team is playing itself$")
	public void the_match_data_provided_contains_an_illegal_match_where_a_team_is_playing_itself()
			throws Throwable {
		Match teamPlayingItself = new Match(Team.BASINGSTOKE.toString(),
				Team.BASINGSTOKE.toString(), 0, 0);
		collectionOfMatches = new ArrayList<Match>();
		collectionOfMatches.add(teamPlayingItself);
	}

	@When("^a league table is requested of the League Table service, the message \"([^\"]*)\" is returned$")
	public void a_league_table_is_requested_of_the_League_Table_service_a_helpful_exception_message_is_returned(
			String helpfulExceptionMessage) throws Throwable {
		String errorMessage = null;
		try {
			LeagueTable service = new LeagueTable(collectionOfMatches);
			LeagueTableCreator creator = new LeagueTableCreator();
			creator.setMatchDataValidator(new MatchDataValidator());
			LeagueTableSorter sorter = new LeagueTableSorter();
			creator.setLeagueTableSorter(sorter);
			service.setLeagueTableCreator(creator);
			service.getTableEntries();
		} catch (IllegalMatchDataException e) {
			errorMessage = e.getMessage();
		}
		assertThat(errorMessage,
				is(equalToIgnoringWhiteSpace((helpfulExceptionMessage))));
	}

	@And("^the match data provided contains a duplicate fixture$")
	public void the_match_data_provided_contains_a_duplicate_fixture()
			throws Throwable {
		Match basingstokeVsAldershot = new Match(Team.BASINGSTOKE.toString(),
				Team.ALDERSHOT.toString(), 4, 1);
		Match basingstokeVsAldershotAgain = new Match(
				Team.BASINGSTOKE.toString(), Team.ALDERSHOT.toString(), 3, 2);
		collectionOfMatches = new ArrayList<Match>();
		collectionOfMatches.add(basingstokeVsAldershot);
		collectionOfMatches.add(basingstokeVsAldershotAgain);

	}

	@And("^the match data provided contains an illegal match where the (home|away) team's name (is null|is empty)$")
	public void the_match_data_provided_contains_an_illegal_match_where_a_team_has_an_empty_name(
			String homeOrAway, String nullOrEmpty) throws Throwable {
		Match malforedMatch = null;

		if (homeOrAway.equals("home")) {
			if (nullOrEmpty.equals("is null")) {
				malforedMatch = new Match(null, "away team", 0, 0);
			} else if (nullOrEmpty.equals("is empty")) {
				malforedMatch = new Match("", "away team", 0, 0);
			}
		} else if (homeOrAway.equals("away")) {
			if (nullOrEmpty.equals("is null")) {
				malforedMatch = new Match("home team", null, 0, 0);
			} else if (nullOrEmpty.equals("is empty")) {
				malforedMatch = new Match("home team", "", 0, 0);
			}
		}

		collectionOfMatches = new ArrayList<Match>();
		collectionOfMatches.add(malforedMatch);
	}

	@When("^two teams are equal on each of points, goal difference and goals for$")
	public void two_teams_are_equal_on_each_of_points_goal_difference_and_goals_for()
			throws Throwable {
		collectionOfMatches = new ArrayList<Match>();
		collectionOfMatches.add(new Match(Team.ALDERSHOT.toString(),
				Team.OVERTON.toString(), 0, 0));
		collectionOfMatches.add(new Match(Team.ALDERSHOT.toString(),
				Team.SWINDON.toString(), 2, 0));
		collectionOfMatches.add(new Match(Team.ALDERSHOT.toString(),
				Team.BASINGSTOKE.toString(), 2, 0));
		collectionOfMatches.add(new Match(Team.ALDERSHOT.toString(),
				Team.READING.toString(), 2, 0));
		collectionOfMatches.add(new Match(Team.ALDERSHOT.toString(),
				Team.WINCHESTER_CITY.toString(), 2, 0));
		collectionOfMatches.add(new Match(Team.OVERTON.toString(),
				Team.ALDERSHOT.toString(), 0, 0));
		collectionOfMatches.add(new Match(Team.OVERTON.toString(), Team.SWINDON
				.toString(), 2, 0));
		collectionOfMatches.add(new Match(Team.OVERTON.toString(),
				Team.BASINGSTOKE.toString(), 2, 0));
		collectionOfMatches.add(new Match(Team.OVERTON.toString(), Team.READING
				.toString(), 2, 0));
		collectionOfMatches.add(new Match(Team.OVERTON.toString(),
				Team.WINCHESTER_CITY.toString(), 2, 0));
	}

	@And("^two teams are equal on both points and goal difference, only$")
	public void two_teams_are_equal_on_both_points_and_goal_difference_only()
			throws Throwable {
		collectionOfMatches.add(new Match(Team.SWINDON.toString(),
				Team.ALDERSHOT.toString(), 0, 0));
		collectionOfMatches.add(new Match(Team.SWINDON.toString(), Team.OVERTON
				.toString(), 2, 0));
		collectionOfMatches.add(new Match(Team.SWINDON.toString(),
				Team.BASINGSTOKE.toString(), 0, 0));
		collectionOfMatches.add(new Match(Team.SWINDON.toString(), Team.READING
				.toString(), 2, 2));
		collectionOfMatches.add(new Match(Team.SWINDON.toString(),
				Team.WINCHESTER_CITY.toString(), 0, 0));
		collectionOfMatches.add(new Match(Team.BASINGSTOKE.toString(),
				Team.ALDERSHOT.toString(), 2, 0));
		collectionOfMatches.add(new Match(Team.BASINGSTOKE.toString(),
				Team.OVERTON.toString(), 0, 0));
		collectionOfMatches.add(new Match(Team.BASINGSTOKE.toString(),
				Team.SWINDON.toString(), 0, 0));
		collectionOfMatches.add(new Match(Team.BASINGSTOKE.toString(),
				Team.READING.toString(), 0, 0));
		collectionOfMatches.add(new Match(Team.BASINGSTOKE.toString(),
				Team.WINCHESTER_CITY.toString(), 0, 0));
	}

	@And("^two teams are equal on points only$")
	public void two_teams_are_equal_on_points_only() throws Throwable {
		collectionOfMatches.add(new Match(Team.READING.toString(),
				Team.ALDERSHOT.toString(), 0, 0));
		collectionOfMatches.add(new Match(Team.READING.toString(), Team.OVERTON
				.toString(), 0, 0));
		collectionOfMatches.add(new Match(Team.READING.toString(), Team.SWINDON
				.toString(), 1, 1));
		collectionOfMatches.add(new Match(Team.READING.toString(),
				Team.BASINGSTOKE.toString(), 1, 1));
		collectionOfMatches.add(new Match(Team.READING.toString(),
				Team.WINCHESTER_CITY.toString(), 2, 0));
		collectionOfMatches.add(new Match(Team.WINCHESTER_CITY.toString(),
				Team.ALDERSHOT.toString(), 0, 0));
		collectionOfMatches.add(new Match(Team.WINCHESTER_CITY.toString(),
				Team.OVERTON.toString(), 0, 0));
		collectionOfMatches.add(new Match(Team.WINCHESTER_CITY.toString(),
				Team.SWINDON.toString(), 2, 2));
		collectionOfMatches.add(new Match(Team.WINCHESTER_CITY.toString(),
				Team.BASINGSTOKE.toString(), 0, 0));
		collectionOfMatches.add(new Match(Team.WINCHESTER_CITY.toString(),
				Team.READING.toString(), 1, 0));
	}

	@When("^a league table is requested from the League Table service, a league table is returned$")
	public void a_league_table_is_requested_from_the_League_Table_service()
			throws Throwable {
		leagueTableService = new LeagueTable(collectionOfMatches);
		LeagueTableCreator creator = new LeagueTableCreator();
		creator.setMatchDataValidator(new MatchDataValidator());
		LeagueTableSorter sorter = new LeagueTableSorter();
		creator.setLeagueTableSorter(sorter);
		leagueTableService.setLeagueTableCreator(creator);
		sortedLeagueTable = leagueTableService.getTableEntries();

		assertThat(sortedLeagueTable.size(), is(6));
	}

	@When("^that league table is sorted by points in descending order$")
	public void that_league_table_is_sorted_by_points_in_descending_order()
			throws Throwable {
		assertThat(sortedLeagueTable.get(0).getPoints(), is(17));
		assertThat(sortedLeagueTable.get(1).getPoints(), is(17));
		assertThat(sortedLeagueTable.get(2).getPoints(), is(10));
		assertThat(sortedLeagueTable.get(3).getPoints(), is(10));
		assertThat(sortedLeagueTable.get(4).getPoints(), is(9));
		assertThat(sortedLeagueTable.get(5).getPoints(), is(9));

	}

	@When("^where two teams in the league have equal points, the teams are sorted instead by goal difference in descending order$")
	public void where_two_teams_in_the_league_have_equal_points_the_teams_are_sorted_instead_by_goal_difference_in_descending_order()
			throws Throwable {
		assertThat(sortedLeagueTable.get(4).getPoints(), is(9));
		assertThat(sortedLeagueTable.get(5).getPoints(), is(9));
		assertThat(sortedLeagueTable.get(4).getGoalDifference(), is(-3));
		assertThat(sortedLeagueTable.get(5).getGoalDifference(), is(-5));
		assertThat(sortedLeagueTable.get(4).getTeamName(),
				is(Team.READING.toString()));
		assertThat(sortedLeagueTable.get(5).getTeamName(),
				is(Team.WINCHESTER_CITY.toString()));

	}

	@When("^where two teams in the league have equal points and equal goal difference, the teams are sorted by goals for, in descending order$")
	public void where_two_teams_in_the_league_have_equal_points_and_equal_goal_difference_the_teams_are_sorted_by_goals_for_in_descending_order()
			throws Throwable {
		assertThat(sortedLeagueTable.get(2).getPoints(), is(10));
		assertThat(sortedLeagueTable.get(3).getPoints(), is(10));
		assertThat(sortedLeagueTable.get(2).getGoalDifference(), is(-2));
		assertThat(sortedLeagueTable.get(3).getGoalDifference(), is(-2));
		assertThat(sortedLeagueTable.get(2).getGoalsFor(), is(7));
		assertThat(sortedLeagueTable.get(3).getGoalsFor(), is(3));
		assertThat(sortedLeagueTable.get(2).getTeamName(),
				is(Team.SWINDON.toString()));
		assertThat(sortedLeagueTable.get(3).getTeamName(),
				is(Team.BASINGSTOKE.toString()));
	}

	@When("^where two teams in the league have equal points and equal goal difference and equal goals for, the teams are sorted by alphabetically, in ascending order$")
	public void where_two_teams_in_the_league_have_equal_points_and_equal_goal_difference_and_equal_goals_for_the_teams_are_sorted_by_alphabetically_in_ascending_order()
			throws Throwable {
		assertThat(sortedLeagueTable.get(0).getPoints(), is(17));
		assertThat(sortedLeagueTable.get(1).getPoints(), is(17));
		assertThat(sortedLeagueTable.get(0).getGoalDifference(), is(6));
		assertThat(sortedLeagueTable.get(1).getGoalDifference(), is(6));
		assertThat(sortedLeagueTable.get(0).getGoalsFor(), is(8));
		assertThat(sortedLeagueTable.get(1).getGoalsFor(), is(8));
		assertThat(sortedLeagueTable.get(0).getTeamName(),
				is(Team.ALDERSHOT.toString()));
		assertThat(sortedLeagueTable.get(1).getTeamName(),
				is(Team.OVERTON.toString()));
	}
}
