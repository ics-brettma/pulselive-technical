package com.pulselive.technicaltest.feature.leaguetable;

/**
 * Test enum for the team names used in the imaginary BDD scenarios football
 * league
 * 
 * @author Mat Brett
 * @since 0.0.1
 *
 */
public enum Team {

	WINCHESTER_CITY("Winchester City"), OVERTON("Overton"), BASINGSTOKE(
			"Basingstoke"), READING("Reading"), ALDERSHOT("Aldershot"), SWINDON(
			"Swindon");

	private String teamName;

	private Team(String teamName) {
		this.teamName = teamName;
	}

	public String getValue() {
		return this.teamName;
	}

	@Override
	public String toString() {
		return this.teamName;
	}
}
