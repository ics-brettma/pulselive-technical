package com.pulselive.technicaltest.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

import com.pulselive.technicaltest.exception.IllegalMatchDataException;
import com.pulselive.technicaltest.feature.leaguetable.Team;
import com.pulselive.technicaltest.model.Match;

/**
 * Unit tests for {code MatchDataValidator}
 * 
 * @author Mat Brett
 * @since 0.0.1
 *
 */
public class MatchDataValidatorTest {

	@Test(expected = IllegalMatchDataException.class)
	public void testIllegalMatchDataExceptionThrownIfNullMatchDataCollectionProvided() {
		new MatchDataValidator().validate(null);
	}

	@Test(expected = IllegalMatchDataException.class)
	public void testIllegalMatchDataExceptionThrownIfEmptyMatchDataCollectionProvided() {
		new MatchDataValidator().validate(new ArrayList<Match>());
	}

	@Test(expected = IllegalMatchDataException.class)
	public void testIllegalMatchDataExceptionThrownIfTeamPlaysItselfInOneFixture() {
		Match teamPlayingThemselves = new Match(Team.BASINGSTOKE.toString(),
				Team.BASINGSTOKE.toString(), 0, 0);
		new MatchDataValidator().validate(Collections
				.singletonList(teamPlayingThemselves));
	}

	@Test(expected = IllegalMatchDataException.class)
	public void testIllegalMatchDataExceptionThrownIfFixtureAppearsInMatchDataMoreThanOnce() {
		Match basingstokeVsAldershot = new Match(Team.BASINGSTOKE.toString(),
				Team.ALDERSHOT.toString(), 4, 1);
		Match basingstokeVsAldershotAgain = new Match(
				Team.BASINGSTOKE.toString(), Team.ALDERSHOT.toString(), 3, 2);
		List<Match> collectionOfMatches = new ArrayList<Match>();
		collectionOfMatches.add(basingstokeVsAldershot);
		collectionOfMatches.add(basingstokeVsAldershotAgain);
		new MatchDataValidator().validate(collectionOfMatches);
	}

	@Test(expected = IllegalMatchDataException.class)
	public void testIllegalMatchDataExceptionThrownIfNullHomeTeamEncountered() {
		new MatchDataValidator().validate(Collections.singletonList(new Match(
				null, "away side", 0, 0)));
	}

	@Test(expected = IllegalMatchDataException.class)
	public void testIllegalMatchDataExceptionThrownIfEmptyHomeTeamEncountered() {
		new MatchDataValidator().validate(Collections.singletonList(new Match(
				"", "away side", 0, 0)));
	}

	@Test(expected = IllegalMatchDataException.class)
	public void testIllegalMatchDataExceptionThrownIfNullAwayTeamEncountered() {
		new MatchDataValidator().validate(Collections.singletonList(new Match(
				"home side", null, 0, 0)));
	}

	@Test(expected = IllegalMatchDataException.class)
	public void testIllegalMatchDataExceptionThrownIfEmptyAwayTeamEncountered() {
		new MatchDataValidator().validate(Collections.singletonList(new Match(
				"home side", "", 0, 0)));
	}

}
