package com.pulselive.technicaltest.services;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThan;

import org.junit.Test;

import com.pulselive.technicaltest.model.LeagueTableEntry;

/**
 * Unit tests for {code LeagueTableSorter}
 * 
 * @author Mat Brett
 * @since 0.0.1
 *
 */
public class LeagueTableSorterTest {

	@Test(expected = NullPointerException.class)
	public void testExceptionThrownWhenComparingToNullAToNonNullB() {
		new LeagueTableSorter().compare(null, new LeagueTableEntry("team name",
				0, 0, 0, 0, 0, 0, 0, 0));

	}

	@Test(expected = NullPointerException.class)
	public void testExceptionThrownWhenComparingToNonNullAToNullB() {
		new LeagueTableSorter().compare(new LeagueTableEntry("team name", 0, 0,
				0, 0, 0, 0, 0, 0), null);

	}

	@Test
	public void testTeamAAboveTeamBIfTeamAHaveMorePoints() {
		LeagueTableEntry teamA = new LeagueTableEntry("Team A", 0, 0, 0, 0, 0,
				0, 0, 10);
		LeagueTableEntry teamB = new LeagueTableEntry("Team B", 0, 0, 0, 0, 0,
				0, 0, 5);
		assertThat(new LeagueTableSorter().compare(teamA, teamB),
				is(lessThan(0)));
	}

	@Test
	public void testTeamBAboveTeamAIfTeamBHaveMorePoints() {
		LeagueTableEntry teamA = new LeagueTableEntry("Team A", 0, 0, 0, 0, 0,
				0, 0, 5);
		LeagueTableEntry teamB = new LeagueTableEntry("Team B", 0, 0, 0, 0, 0,
				0, 0, 10);
		assertThat(new LeagueTableSorter().compare(teamA, teamB),
				is(greaterThan(0)));
	}

	@Test
	public void testTeamAAboveTeamBIfBothTeamsHaveTheSamePointsButTeamAHasAGreaterGoalDifference() {
		LeagueTableEntry teamA = new LeagueTableEntry("Team A", 0, 0, 0, 0, 0,
				0, 20, 10);
		LeagueTableEntry teamB = new LeagueTableEntry("Team B", 0, 0, 0, 0, 0,
				0, 10, 10);
		assertThat(new LeagueTableSorter().compare(teamA, teamB),
				is(lessThan(0)));
	}

	@Test
	public void testTeamAIsAboveTeamBIfBothTeamsHaveTheSamePointsAndtheSameGoalDifferenceButTeamAHasScoredMoreGoals() {
		LeagueTableEntry teamA = new LeagueTableEntry("Team A", 0, 0, 0, 0, 50,
				0, 20, 10);
		LeagueTableEntry teamB = new LeagueTableEntry("Team B", 0, 0, 0, 0, 25,
				0, 20, 10);
		assertThat(new LeagueTableSorter().compare(teamA, teamB),
				is(lessThan(0)));
	}

	@Test
	public void testTeamAIsAboveTeamBIfBothTeamsHaveTheSamePointsGoalDifferenceAndGoalsForButTeamAIsAlphabeticallyBeforeTeamB() {
		LeagueTableEntry teamA = new LeagueTableEntry("Aldershot", 0, 0, 0, 0,
				50, 0, 20, 10);
		LeagueTableEntry teamB = new LeagueTableEntry("Overton", 0, 0, 0, 0,
				50, 0, 20, 10);
		assertThat(new LeagueTableSorter().compare(teamA, teamB),
				is(lessThan(0)));
	}
}
